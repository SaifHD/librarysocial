<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $fillable=[
        'name',
        'isbn_number',
        'author',
        'released_date',
        'description',
        'user_id'
    ];
    // Relationship between book and owner
    public function owner(){
        return $this->belongsTo(User::class,'owner_id');
    }

    // Relationship between book and requested user
    public function users(){
        return $this->belongsToMany(User::class)->withTimestamps()->withPivot('status')->latest();
    }

}
