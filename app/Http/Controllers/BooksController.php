<?php

namespace App\Http\Controllers;

use App\Http\Requests\BookStoreRequest;
use Illuminate\Http\Request;
use App\Models\Book;
use App\Notifications\BookRequestNotification;

class BooksController extends Controller
{
    public function index()
    {
        $books = Book::latest()->with('owner')->paginate(10);
        // dd($books);
        return view('book.books', compact('books'));
    }

    public function create()
    {
        // dd("create");
        return view('book.create');
    }

    public function store(BookStoreRequest $request)
    {
        // dd($request->all());
        $book = new Book();
        $book->name          = $request->name;
        $book->isbn_number   = $request->isbn_number;
        $book->author        = $request->author;
        $book->released_date = $request->released_date;
        $book->description   = $request->description;
        $book->owner_id      = auth()->user()->id;
        if ($book->save()) {
            return redirect()->route('books')->with('success', 'Your Book SuccessfullyStored');
        }

        return redirect()->back()->with('error', 'Oops! Not stored');
    }

    public function show($id)
    {
        $book = Book::with(['owner', 'users'])->findOrFail($id);


        return view('book.show_book', compact('book'));
    }
    public function request(Request $request)
    {
        $book = Book::findOrFail($request->book_id);

        $book->users()->attach([auth()->user()->id]);

        $owner = $book->owner;

        // dd($book->name);

        $owner->notify(new BookRequestNotification($book, auth()->user()));
        return redirect()->back()->with('success', 'Your Request Sent Successfully');
    }

    public function status($id,$user_id){
        $book=Book::findOrFail($id);
        $user=$book->users->where('id',$user_id)->first();
        if($user->pivot->status==0){
            $book->users()->detach($user);
            $book->users()->attach($user->id,['status'=>1]);
        }
        else{
            $book->users()->detach($user);
            $book->users()->attach($user->id, ['status' => 0]);
        }
        return redirect()->back();
    }
}
