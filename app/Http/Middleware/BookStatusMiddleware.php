<?php

namespace App\Http\Middleware;

use App\Models\Book;
use Closure;
use Illuminate\Http\Request;

class BookStatusMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $book=Book::find($request->id);
        if($book->owner_id==auth()->user()->id){
            return $next($request);
        }
        else{
            return redirect()->back();
        }

    }
}
