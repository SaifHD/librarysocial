@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"><h4>Create New Book</h4></div>

                <div class="card-body">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif

                    <form action={{ route('books.store') }} method="Post">
                        @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" name="name"  placeholder="Enter Name of the Book" value="{{ old('name') }}">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">ISBN Number</label>
                            <input type="text" class="form-control" name="isbn_number" placeholder="Enter ISBN of the book" value="{{ old('isbn_number') }}">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Author</label>
                            <input type="text" class="form-control" name="author" placeholder="Author of the Book" value="{{ old('author') }}">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Released Date</label>
                            <input type="date" class="form-control" name="released_date" placeholder="Released Date of the book" value="{{ old('released_date') }}">

                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea name="description" class="form-control" id="" cols="30" rows="2"></textarea>


                        </div>


                        <button type="submit" class="btn btn-primary">Submit</button>
                        </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
