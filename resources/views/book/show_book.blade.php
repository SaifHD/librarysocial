@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4>Details of {{ $book->name }}</h4></div>
                <div class="card-body">
                    <ul class="list-group mb-5">
                        <li class="list-group-item">Name <span style="float:right;">{{ $book->name }}</span></li>
                        <li class="list-group-item">ISBN Number <span style="float:right;">{{ $book->isbn_number }}</span></li>
                        <li class="list-group-item">Author <span style="float:right;">{{ $book->author }}</span></li>
                        <li class="list-group-item">Released Date <span style="float:right;">{{ $book->released_date }}</span></li>
                        <li class="list-group-item">Now Owned By <span style="float:right;">{{ $book->owner->name }}</span></li>
                        <li class="list-group-item">Description <span style="float:right;">{{ $book->description }}</span></li>
                        <li class="list-group-item">Request <span style="float:right;">
                            @if($book->owner->id==auth()->user()->id)
                                You are the Owner
                            @else
                                @if(count($book->users->where('id',auth()->user()->id))>0)
                                    Requested
                                @else
                                    <form action="{{ route('books.request') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="book_id" value="{{ $book->id }}">
                                        <button type="submit" class="btn btn-sm btn-success">Request Book</button>
                                    </form>
                                @endif
                            @endif
                        </span></li>
                    </ul>
                    <h4>Requests Table</h4>
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">User Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Requested at</th>
                                <th scope="col">Status</th>
                                @if($book->owner_id==auth()->user()->id)
                                    <th scope="col">Action</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($book->users)>0)
                                @foreach ($book->users as $user)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $user->name }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->pivot->created_at }}</td>
                                        <td>
                                            @if($user->pivot->status==0)
                                                <span class="badge badge-info">Not Delevered</span>
                                            @elseif($user->pivot->status==1)
                                                <span class="badge badge-success">Delevered</span>
                                            @endif
                                        </td>
                                        @if($book->owner_id==auth()->user()->id)
                                            <td>
                                                @if($user->pivot->status==0)
                                                    <a href="{{ route('books.status',[$book->id,$user->id]) }}" class="btn btn-sm btn-success">Delever</a>
                                                @elseif($user->pivot->status==1)
                                                    <a href="{{ route('books.status',[$book->id,$user->id]) }}" class="btn btn-sm btn-danger">Cancel</a>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center" >
                                    <th scope="row" class="text-center" colspan="6">No Records There</th>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
