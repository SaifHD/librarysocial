@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4>All Books</h4></div>

                <div class="card-body">
                    @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    <table class="table">
                        <thead>
                            <tr>
                            <th scope="col">#</th>
                            <th scope="col">Book Name</th>
                            <th scope="col">Book Owned By</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(count($books)>1)
                                @foreach ($books as $book)
                                    <tr>
                                        <th scope="row">
                                            @if(Request::get('page')>1)
                                                {{ $loop->iteration+(Request::get('page')-1)*10 }}
                                            @else
                                                {{ $loop->iteration }}
                                            @endif
                                        </th>
                                        <td>
                                            <a href="{{ route('books.show',$book->id) }}">{{ $book->name }}</a></td>
                                        <td>{{ $book->owner->name }}</td>
                                        <td>
                                            <a href="{{ route('books.show',$book->id) }}" class="btn btn-info btn-sm">Details</a>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif


                        </tbody>

                    </table>
                    @if(count($books)>1)
                    <div class="mt-5" style="float:right;">
                        {{ $books->links() }}
                    </div>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
