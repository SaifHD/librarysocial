<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::middleware(['auth'])->group(function () {
    Route::middleware(['book_status'])->group(function () {
        Route::get('books/{id}/status/{user_id}', [App\Http\Controllers\BooksController::class, 'status'])->name('books.status');
    });

    Route::post('books/request', [App\Http\Controllers\BooksController::class, 'request'])->name('books.request');
    Route::get('books', [App\Http\Controllers\BooksController::class, 'index'])->name('books');
    Route::get('books/create', [App\Http\Controllers\BooksController::class, 'create'])->name('books.create');
    Route::post('books', [App\Http\Controllers\BooksController::class, 'store'])->name('books.store');
    Route::get('books/{id}', [App\Http\Controllers\BooksController::class, 'show'])->name('books.show');

});

