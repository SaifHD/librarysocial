<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $user=new User();
        $user->name="Admin";
        $user->email="admin@gmail.com";
        $user->password=Hash::make('admin1234');
        $user->save();
        \App\Models\Book::factory(20)->create([
            'owner_id'=>$user->id,
        ]);
        $user1 = new User();
        $user1->name = "user";
        $user1->email = "user@gmail.com";
        $user1->password = Hash::make('user1234');
        $user1->save();
        
    }
}
